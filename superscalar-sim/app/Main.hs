module Main where

import Lib
import Pretty

import Data.Foldable
import System.Environment

main :: IO ()
main = do
  args <- getArgs
  case args of
    [file,numWaySuperscalar, numALUs] -> 
      simulateFilePrintStepsStatsConfig (read numWaySuperscalar) (read numALUs) file
    [file,numWaySuperscalar] -> 
      simulateFilePrintStepsStatsConfig (read numWaySuperscalar) (read numWaySuperscalar) file  
    [file] ->
      simulateFilePrintStepsStatsConfig 4 4 file
    _ -> do
      putStrLn "Incorrect input, please use the following format:"
      putStrLn "stack exec superscalar-sim-exe -- file [numSuperscalar] [numALUs]"

