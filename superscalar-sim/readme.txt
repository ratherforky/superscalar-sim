1. Download source files
2. In this directory, invoke `stack build` (this may take a while)
3. Run `stack exec superscalar-sim-exe -- testPrograms/factorialUnroll.asm`
   (this will produce a lot of output, I recommend piping it into `less`)
4. Optionally, the simulator can be ran N-way superscalar using
   `stack exec superscalar-sim-exe -- [file] [numWaySuperscalar]`
5. Number of ALUs can be specified with
  `stack exec superscalar-sim-exe -- [file] [numWaySuperscalar] [numALUs]`