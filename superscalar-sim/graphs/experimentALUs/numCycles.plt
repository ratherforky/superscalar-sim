set title "Number of cycles against number of ALUs"
set xlabel "Number of ALUs"
set ylabel "Number of cycles"
set yrange [0:600]
set key right center
plot NaN title "factorialUnroll" w line lt 1 lc 5 lw 6,\
 'data/factorialUnroll' using 1:3 with lines title "" lt 1 lc 5 lw 3,\
 NaN title "WAWDependencies" w line lt 1 lc 4 lw 6,\
 'data/WAWDependencies' using 1:3 with lines title "" lt 1 lc 4 lw 3,\
 NaN title "WARDependencies" w line lt 1 lc 3 lw 6,\
 'data/WARDependencies' using 1:3 with lines title "" lt 1 lc 3 lw 3,\
 NaN title "RAWDependencies" w line lt 1 lc 2 lw 6,\
 'data/RAWDependencies' using 1:3 with lines title "" lt 1 lc 2 lw 3