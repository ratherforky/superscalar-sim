set title "Number of cycles for a 500 instruction program\nfor true dependencies vs false dependencies"
set nokey
set yrange [0:600]
set boxwidth 2
set style fill solid
set ylabel "Number of cycles"
plot "dependencies.dat" using 2:xtic(1) with histograms
