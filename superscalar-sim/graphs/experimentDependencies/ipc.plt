set title "Instructions Per Cycle for true dependencies vs false dependencies"
set nokey
set yrange [0:4]
set boxwidth 2
set style fill solid
set ylabel "Instructions Per Cycle"
plot "dependencies.dat" using 3:xtic(1) with histograms
