set title "Number of cycles against N-way superscalar"
set xlabel "N-way superscalar"
set ylabel "Number of cycles"
set yrange [0:600]
set xrange [1:16]
set key right center
plot NaN title "factorialUnroll" w line lt 1 lc 5 lw 6,\
 'factorialUnroll.issuePerCycle' using 1:3 with lines title "" lt 1 lc 5 lw 3,\
 NaN title "WAWDependencies" w line lt 1 lc 4 lw 6,\
 'WAWDependencies.issuePerCycle' using 1:3 with lines title "" lt 1 lc 4 lw 3,\
 NaN title "WARDependencies" w line lt 1 lc 3 lw 6,\
 'WARDependencies.issuePerCycle' using 1:3 with lines title "" lt 1 lc 3 lw 3,\
 NaN title "RAWDependencies" w line lt 1 lc 2 lw 6,\
 'RAWDependencies.issuePerCycle' using 1:3 with lines title "" lt 1 lc 2 lw 3