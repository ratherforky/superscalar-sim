module Utilities where

import Prelude as P

import Types

import Control.Monad.Trans.Reader
import qualified Data.Bimap as B
import Data.Functor.Identity
import Data.Bits.Extras
import Data.Int
import Data.Maybe (fromJust)
import Lens.Micro ((^.), (.~), (%~), (+~), Getting)

import qualified Data.IntMap.Strict as IM
import qualified Data.Sequence as S
import qualified Data.Vector as V

import qualified Data.ReorderBuffer as RB

import Initialise

-- update :: Integral a => Registers -> Register -> a -> Registers
-- update s r x = s V.// [(regToIndex r, w64 x)]

update :: Integral a => ArchRegister -> a -> Registers -> Registers
update r x s = s V.// [(regToIndex r, w64 x)]

-- update'' :: Integral a => Register -> a -> CPU ()
-- update'' r x = modify' (regs %~ (V.// [(regToIndex r, w64 x)]))

regToIndex :: ArchRegister -> Int
regToIndex = fromEnum

askFromReg' :: Registers -> ArchRegister -> Word64
askFromReg' s r = s V.! regToIndex r

askFromReg :: ArchRegister -> CPU Word64
askFromReg r = do
  regs <- askRegs
  return $ regs V.! regToIndex r

askRAT :: CPU RegisterAliasTable
askRAT = askLens rat

askForOperand :: ArchRegister -> CPU Operand
askForOperand r = do
  cRAT <- askRAT
  case B.lookup r cRAT of
    Just robEntry -> getOperand robEntry
    Nothing       -> Val <$> askFromReg r

getOperand :: ROBEntry -> CPU Operand
getOperand robEntry = do
  cROB <- askROB
  case RB.indexROB cROB robEntry of
    Just (ROBContents _ (Just v)) -> pure $ Val v
    _                             -> pure $ ROB robEntry

askRegs :: CPU Registers
askRegs = askLens regs 

askIssuePerCycle :: CPU Int
askIssuePerCycle = askLens issuePerCycle

askALUs :: CPU [ALU]
askALUs = askLens (aluPL . aluEUs)

askCDB :: CPU CommonDataBus
askCDB = askLens cdb

askRS :: CPU (ReservationStation (ALUInstr Operand))
askRS = askLens (aluPL . aluRS)

askROB :: CPU (ReorderBuffer ROBContents)
askROB = askLens rob

askLens :: Getting a ProcState a -> CPU a
askLens lens = asks (^. lens)

data HSISFuncs a b = HSISFuncs
  { add  :: a -> a -> a -> b
  , addi :: a -> a -> Word64 -> b
  , sub  :: a -> a -> a -> b
  , subi :: a -> a -> Word64 -> b
  , mul  :: a -> a -> a -> b
  , cmp  :: a -> a -> a -> b
  , lw   :: a -> a -> a -> b
  , ldc  :: a -> Word64 -> b
  , sw   :: a -> a -> a -> b
  , mov  :: a -> a -> b
  , b    :: Word64 -> b
  , j    :: Word64 -> b
  , blth :: a -> a -> Word64 -> b
  , noop :: b
  }

foldHSIS :: HSISFuncs a b -> HSIS a -> b
foldHSIS f (ADD x y z)  = add f x y z
foldHSIS f (ADDi x y z) = addi f x y z
foldHSIS f (SUB  x y z) = sub f x y z
foldHSIS f (SUBi x y z) = subi f x y z
foldHSIS f (MUL  x y z) = mul f x y z
foldHSIS f (CMP  x y z) = cmp f x y z
foldHSIS f (LW   x y z) = lw f x y z
foldHSIS f (LDC  x y)   = ldc f x y
foldHSIS f (SW   x y z) = sw f x y z
foldHSIS f (MOV  x y)   = mov f x y
foldHSIS f (B x)        = b f x
foldHSIS f (J x)        = j f x
foldHSIS f (BLTH x y z) = blth f x y z
foldHSIS f NOOP         = noop f

-- ROB

hsisToROBContents :: HSIS ArchRegister -> ROBContents
hsisToROBContents = foldHSIS funcs
  where
    funcs :: HSISFuncs ArchRegister ROBContents
    funcs = HSISFuncs
      { add  = \dest y z -> ROBContents {_reg=dest, _val=Nothing}
      , addi = \dest y z -> ROBContents {_reg=dest, _val=Nothing}
      , sub  = \dest y z -> ROBContents {_reg=dest, _val=Nothing}
      , subi = \dest y z -> ROBContents {_reg=dest, _val=Nothing}
      , mul  = \dest y z -> ROBContents {_reg=dest, _val=Nothing}
      , cmp  = \dest y z -> ROBContents {_reg=dest, _val=Nothing}
      , lw   = \_ _ _    -> error "ROB not defined for LW"
      , ldc  = \dest x -> ROBContents {_reg=dest, _val=Nothing}
      , sw   = \_ _ _ -> error "ROB not defined for LW"
      , mov  = \dest y -> ROBContents {_reg=dest, _val=Nothing}
      , b    = \_ -> error "ROB not defined for LW"
      , j    = \_ -> error "ROB not defined for LW"
      , blth = \_ _ _ -> error "ROB not defined for LW"
      , noop = error "ROB not defined for LW"
      }

-- Misc

seqHead :: Seq a -> Maybe a
seqHead = S.lookup 0

applyAll :: a -> [a->a] -> a
applyAll = foldr ($)

loadInstrs :: ProcState -> [(HSIS ArchRegister)] -> ProcState
loadInstrs s instructions = s { _instrCache = S.fromList instructions }

-- loadInstrs' :: Vector (HSIS ArchRegister) -> CPU ()
-- loadInstrs' instructions = modify' (instrs .~ instructions)

halted :: ProcState -> Bool
halted ps = fetchEmpty && robEmpty && pcAtEndOfInstructions
  where
  fetchEmpty = S.null (ps ^. fpdUnit . window)
  robEmpty = RB.isEmpty (ps ^. rob)
  pcAtEndOfInstructions = (ps ^. pc) >= S.length (ps ^. instrCache)

-- execCPU :: CPU a -> ProcState
-- execCPU cpu = runIdentity $ execStateT cpu empty

-- evalCPU :: CPU a -> a
-- evalCPU cpu = runIdentity $ evalStateT cpu empty

-- Convenience functions

int :: Integral a => a -> Int
int = fromIntegral

i64 :: Integral a => a -> Int64
i64 = fromIntegral

isEmpty :: String -> Bool
isEmpty "" = True
isEmpty _  = False

vUnlines :: Vector String -> String
vUnlines = unlines . V.toList

-- Debug functions

printUnlines :: Show a => [a] -> IO ()
printUnlines = putStrLn . unlines . map show

-- Examples

exampleROB :: Maybe (ReorderBuffer ROBContents, ROBEntry)
exampleROB = do
  rob <- RB.allocateList (RB.emptyROBWithSize 10) $ map (ROBContents R12 . Just) [1..5]
  RB.allocateGetIndex (ROBContents R0 (Just 42)) rob

exampleProcState :: ProcState
exampleProcState = initialise { _rob = fst $ fromJust exampleROB }
