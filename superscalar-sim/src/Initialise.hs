{-# LANGUAGE TypeSynonymInstances, FlexibleInstances #-}
module Initialise where

import Types

import qualified Data.Bimap as B (empty)
import qualified Data.Memory as M (empty)
import qualified Data.Sequence as S (empty)
import Data.ReorderBuffer (emptyROBWithSize)
import Data.ReservationStation (emptyRSWithSize)
import qualified Data.Vector as V (replicate)

class Initialise a where
  initialise :: a

instance Initialise ProcState where
  initialise = PS
    { _instrCache = initialise
    , _fpdUnit    = initialise
    , _rat        = initialise
    , _rob        = initialise
    , _aluPL      = initialise
    , _cdb        = initialise
    , _regs       = initialise
    , _mem        = initialise
    , _pc         = initialise
    , _issuePerCycle = 4
    }

instance Initialise InstructionCache where
  initialise = S.empty

instance Initialise FetchPreDecodeUnit where
  initialise = FPDU { _window = S.empty }

instance Initialise RegisterAliasTable where
  initialise = B.empty

instance Initialise (ReorderBuffer ROBContents) where
  initialise = emptyROBWithSize 32

instance Initialise ALUPipeline where
  initialise = ALUPL
    { _aluRS  = initialise
    , _aluEUs = initialise
    }

instance Initialise (ReservationStation (ALUInstr Operand)) where
  initialise = emptyRSWithSize 16

instance Initialise [ALU] where
  initialise = replicate 4 initialise

instance Initialise ALU where
  initialise = ALU { _instr = Nothing }

instance Initialise CommonDataBus where
  initialise = CDB { _aluBroadcasts = [] }

instance Initialise Registers where
  initialise = V.replicate 16 0

instance Initialise Memory where
  initialise = M.empty

instance Initialise Int where
  initialise = 0