{-# LANGUAGE FlexibleInstances, RecordWildCards #-}
module Pretty where

import Types
import Utilities

import Data.ReservationStation (foldRS)
import Data.Memory (foldMemory)
import Data.ReorderBuffer (foldrWithEntry)

import qualified Data.Bimap as B
import qualified Data.Foldable as F
import qualified Data.Sequence as S
import qualified Data.Vector as V

class Pretty a where
  pretty :: a -> String

instance Pretty ProcState where
  pretty PS{..} = unlines [ "==============================================================================="
                          , pretty _pc, ""
                          , pretty _instrCache
                          , pretty _fpdUnit
                          , pretty _rat
                          , pretty _rob
                          , pretty _aluPL
                          , pretty _cdb, ""
                          , pretty _regs
                          , pretty _mem
                          , "==============================================================================="
                          ]

instance Pretty InstructionCache where
  pretty instrs = unlines [ "Instruction cache:"
                          , show $ F.toList instrs ]

instance Pretty FetchPreDecodeUnit where
  pretty (FPDU window) = unlines [ "Fetch/Pre-decode Window:"
                                 , show $ F.toList window ]

instance Pretty RegisterAliasTable where
  pretty rat = unlines $ [ "RAT:", ""                   
                         , " Reg | ROB Entry "
                         , "-----------------" ]
                      ++ map (row rat) [R0 .. LR]
    where
      row' :: (ArchRegister, String) -> String
      row' (r, robEntry) = concat [" ", pretty r , " |    ", robEntry ]

      row :: RegisterAliasTable -> ArchRegister -> String
      row rat r = case B.lookup r rat of
                    Just robEntry -> row' (r, show robEntry)
                    Nothing       -> row' (r, "None")

instance Pretty ArchRegister where
  pretty R10 = "R10"
  pretty R11 = "R11"
  pretty R12 = "R12"
  pretty r   = show r ++ " "

instance Pretty (ReorderBuffer ROBContents) where
  pretty rob = unlines [ "Reorder Buffer:\n"
                       , " Entry | Reg | Val"
                       , "----------------------"]
            ++ foldrWithEntry f "" rob
    where
      f :: ROBEntry -> ROBContents -> String -> String
      f robEntry robContents acc = " " ++ show robEntry ++ "   | " ++ pretty robContents ++ "\n" ++ acc

instance Pretty ROBContents where
  pretty ROBContents{..} = " " ++ pretty _reg ++ " | " ++ show _val

instance Pretty ALUPipeline where
  pretty ALUPL{..} = unlines [ pretty _aluRS
                             , pretty _aluEUs 
                             ]

instance Pretty (ReservationStation (ALUInstr Operand)) where
  pretty rs = unlines [ "ALU Reservation Station:"
                      , ""
                      , " ROB Entry | Instruction"
                      , "-------------------------"
                      , foldRS f "" rs
                      ]
    where
      f :: (ROBEntry, ALUInstr Operand) -> String -> String
      f (robEntry, instr) acc = concat [" ", show robEntry, spaces robEntry, "| ", show instr, "\n", acc]

      spaces :: ROBEntry -> String
      spaces rob
        | rob >= 0 && rob < 10 = "         "
        | otherwise            = "        "

instance Pretty [ALU] where
  pretty alus = "ALUs:\n" ++ snd (foldr f (1,"") alus)
    where
      f :: ALU -> (Int, String) -> (Int, String)
      f alu (n, acc) = (n+1, concat [show n, ": ", pretty alu, "\n", acc])

instance Pretty ALU where
  pretty ALU{..} = case _instr of
                     Just aluinstr -> show aluinstr
                     Nothing       -> "No instruction"

instance Pretty CommonDataBus where
  pretty CDB{..} = "Common Data Bus: " ++ show _aluBroadcasts

instance Pretty Registers where
  pretty regs = concat [ "Registers:\n"
                       , vUnlines $ V.zipWith f (V.fromList [R0 .. LR]) regs
                       ]
    where
      f :: ArchRegister -> Word64 -> String
      f r w = show r ++ ":" ++ spaces r ++ show w

      spaces r
        | r >= R10 && r <= R12 = " "
        | otherwise            = "  "

instance Pretty Memory where
  pretty mem = unlines [ "Memory:"
                       , foldMemory f "Empty" mem
                       ]
    where
      f :: Int -> Word64 -> String -> String
      f addr val "Empty" = concat [show addr, " -> ", show val, "\n"]
      f addr val acc     = concat [show addr, " -> ", show val, "\n", acc]

instance Pretty PC where
  pretty pc = "Program Counter: " ++ show pc

instance Pretty Program where
  pretty (Program xs) = concatMap (\l -> show l ++ "\n") xs

prettyPrint :: Pretty a => a -> IO ()
prettyPrint = putStrLn . pretty
  