{-# LANGUAGE TemplateHaskell #-}
module Types
  ( HSIS(..), ArchRegister(..), ProcState(..), CPU
  , Registers, InstructionCache, PC
  , ROBContents(..), RegisterAliasTable, CommonDataBus(..), FetchPreDecodeUnit(..)
  , InstrType(..)
  , ALUPipeline(..), ALU(..), ALUInstr(..)
  , Vector, Word64, Seq, Bimap
  , instrCache, fpdUnit, rat, rob, aluPL, cdb, regs, mem, pc -- Procstate lenses
  , reg, val -- ROBContents lenses
  , aluBroadcasts -- CommonDataBus lenses
  , window -- FetchPreDecodeUnit lenses
  , aluRS, aluEUs -- ALUPipeline lenses
  , instr -- ALU lenses
  , issuePerCycle -- Lens to issues per cycle
  , Program(..), Line(..), Macro(..), Statement(..), Instruction(..)
  , Comment, Identifier, Reglike, Imm32, Imm64, Label
  , ReorderBuffer, ROBEntry, ReservationStation, Operand(..), Memory
  ) where

import Control.Monad.Trans.Reader (Reader)
import Data.Functor.Identity
import Data.Int
import Data.Bimap (Bimap)
import Data.Sequence (Seq)
import Data.Vector (Vector)
import Data.Word (Word64)
import Lens.Micro.TH (makeLenses)

import Data.Memory (Memory)
import Data.ReorderBuffer (ReorderBuffer, ROBEntry)
import Data.ReservationStation (ReservationStation, Operand(..), ALUInstr(..))

-- Haskell Simulator Instruction Set
data HSIS operand 
  = ADD  operand operand operand
  | ADDi operand operand Word64
  | SUB  operand operand operand
  | SUBi operand operand Word64
  | MUL  operand operand operand
  -- Compare (result -1, 0, or +1 for LT, EQ, or GT respectively)
  | CMP  operand operand operand
  -- Load dest baseAddress offset
  | LW   operand operand operand
  -- Load constant
  | LDC  operand Word64
  -- Store word
  -- SW data baseAddress offset
  | SW   operand operand operand
  -- Copy from R2 to R1
  | MOV  operand operand
  -- Absolute branch
  | B    Word64
  -- Jump (relative branch)
  | J    Word64
  -- Absolute branch when r1 < r2
  | BLTH operand operand Word64
  -- No operation
  | NOOP
  deriving (Show, Read)

data ArchRegister
  = R0
  | R1
  | R2
  | R3
  | R4
  | R5
  | R6
  | R7
  | R8
  | R9
  | R10
  | R11
  | R12
  | SP -- Stack Pointer
  | LR -- Link Register
  deriving (Enum, Eq, Ord, Show, Read)

data ProcState = PS 
  { _instrCache :: InstructionCache
  , _fpdUnit    :: FetchPreDecodeUnit
  , _rat        :: RegisterAliasTable
  , _rob        :: ReorderBuffer ROBContents
  , _aluPL      :: ALUPipeline
  , _cdb        :: CommonDataBus
  , _regs       :: Registers
  , _mem        :: Memory
  , _pc         :: Int
  , _issuePerCycle :: Int
  }
  deriving Show

type PC = Int
type Registers = Vector Word64
type InstructionCache = Seq (HSIS ArchRegister)

data ROBContents = ROBContents
  { _reg :: ArchRegister
  , _val :: Maybe Word64
  }
  deriving Show

-- Maps architectural registers to ROB entries and vice versa
type RegisterAliasTable = Bimap ArchRegister ROBEntry

-- TODO: Come up with actual types for these
-- type LoadStorePipeline = ()
data CommonDataBus = CDB
  { _aluBroadcasts :: [(ROBEntry, Word64)]
  }
  deriving Show

data FetchPreDecodeUnit = FPDU 
  { _window :: Seq (HSIS ArchRegister, InstrType) }
  deriving Show

data InstrType = Branch
               | Arithmetic
               | LoadStore
               | Misc
               deriving (Show, Eq)

data ALUPipeline = ALUPL
  { _aluRS  :: ReservationStation (ALUInstr Operand)
  , _aluEUs :: [ALU]
  }
  deriving Show

data ALU = ALU
  { _instr    :: Maybe (ROBEntry, ALUInstr Word64)
  }
  deriving Show

-- TODO: Should be reader for current state, new state only made at end of cycle
type CPU a = Reader ProcState a

makeLenses ''ProcState
makeLenses ''ROBContents
makeLenses ''CommonDataBus
makeLenses ''FetchPreDecodeUnit
makeLenses ''ALUPipeline
makeLenses ''ALU

-- Execution units:
-- Integer Units
-- Load/Store Units
-- Double Precision Floating Point units
-- Branch unit
-- Vector unit
-- Decimal floating point ?

-- Parsing types

newtype Program = Program [Line]
  deriving (Show, Eq)

data Line
  = Def Macro
  | Stm Statement
  | Com Comment
  | Empty
  deriving (Show, Eq)

data Macro
  = Macro Identifier Int32
  deriving (Show, Eq)

data Statement
  = Labled Label Instruction
  | Unlabled Instruction
  deriving (Show, Eq)

type Comment    = String
type Identifier = String
type Reglike    = Either ArchRegister Identifier
type Imm32      = Either Int32 Identifier
type Imm64      = Either Word64 Label
type Label      = String

data Instruction 
  = PADD  Reglike Reglike Reglike
  | PADDi Reglike Reglike Imm32
  | PSUB  Reglike Reglike Reglike
  | PSUBi Reglike Reglike Imm32
  | PMUL  Reglike Reglike Reglike
  | PDIV  Reglike Reglike Reglike
  -- Compare (result -1, 0, or +1 for LT, EQ, or GT respectively)
  | PCMP  Reglike Reglike Reglike
  -- Load dest baseAddress offset
  | PLW   Reglike Reglike Reglike
  -- Load constant
  | PLDC  Reglike Imm32
  -- Store word
  -- SW data baseAddress offset
  | PSW   Reglike Reglike Reglike
  -- Copy from R2 to R1
  | PMOV  Reglike Reglike
  -- Absolute branch
  | PB    Imm64
  -- Jump (relative branch)
  | PJ    Word64
  -- Absolute branch when r1 < r2
  | PBLTH Reglike Reglike Imm32
  -- No operation
  | PNOOP
  deriving (Show, Read, Eq)
