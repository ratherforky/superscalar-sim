module Lib
    ( simulateFilePrintStepsStatsConfig
    ) where

import Initialise
import Types
import Utilities
import Pretty
import Parser

import Data.ReorderBuffer
import Data.ReservationStation

import qualified Data.Bimap as B
import Data.Bits.Extras
import Data.Foldable (traverse_)
import Data.Int
import Data.List (foldl')
import Data.List.HT (takeUntil)
import Data.Maybe
import Data.Sequence ((><))
import qualified Data.Sequence as S

import Control.Monad.Trans.Reader (runReader, ask, local)

import Lens.Micro ((^.), (.~), (%~), (+~), _1, _2)

-- Processor logic

tick :: (Int, ProcState) -> (Int, ProcState)
tick (cycleCount, s) = (cycleCount + 1, runReader tickReader s)

printTick :: (Int, ProcState) -> IO (Int, ProcState)
printTick s@(_, ps) = do
  prettyPrint ps
  pure $ tick s

-- This function would be much more readable with a State monad, apologies
tickReader :: CPU ProcState
tickReader = do
  (regs', rat', rob') <- commit
  let transformCommit = (regs .~ regs') 
                      . (rat  .~ rat')
                      . (rob  .~ rob')
  (alus', cdb', rs', rob'') <- local transformCommit executeWriteback
  let transformExecuteWriteback = ((aluPL . aluRS) .~ rs')
                                . (rob .~ rob'') 
                                . ((aluPL . aluEUs) .~ alus')
                                . (cdb .~ cdb')
                                . transformCommit
  (rs'', alus'') <- local transformExecuteWriteback dispatchRS
  let transformDispatch = ((aluPL . aluRS) .~ rs'')
                        . ((aluPL . aluEUs) .~ alus'')
                        . transformExecuteWriteback
  (window', rob''', aluRS', rat'') <- local transformDispatch decodeIssue
  let transformDecode = ((fpdUnit . window) .~ window')
                      . (rob .~ rob''')
                      . ((aluPL . aluRS) .~ aluRS')
                      . (rat .~ rat'')
                      . transformDispatch
  (fpdUnit', pc') <- local transformDecode fetch
  let transformFetch = (fpdUnit .~ fpdUnit') 
                     . (pc .~ pc')
                     . transformDecode
  
  current <- ask
  return $ transformFetch current

-------------------------------------------------------------------------------
-- Fetch/Pre-decode 
-------------------------------------------------------------------------------

fetch :: CPU (FetchPreDecodeUnit, PC)
fetch = do
  current <- ask
  issPerCycle <- askIssuePerCycle
  let cInstrCache = current ^. instrCache
      cPC         = current ^. pc
      (_, nextInstrs) = S.splitAt cPC cInstrCache
      cWindow     = current ^. fpdUnit . window
      freeSpaces  = issPerCycle - S.length cWindow
      window'
        | freeSpaces < 0  = error "Can't be less than 0 in window"
        | freeSpaces > issPerCycle  = error $ "Can't be more than " ++ show issPerCycle ++ " in window"
        | otherwise = cWindow >< fmap preDecode (S.take freeSpaces nextInstrs) -- Sliding window
      finalPC = S.length (current ^. instrCache)
      pc' 
        | cPC + freeSpaces > finalPC = finalPC
        | otherwise = cPC + freeSpaces
      fpdUnit' = window .~ window' $ current ^. fpdUnit
  return $ (fpdUnit', pc')

preDecode :: HSIS ArchRegister -> (HSIS ArchRegister, InstrType) 
preDecode i@(ADD  _ _ _) = (i, Arithmetic)
preDecode i@(ADDi _ _ _) = (i, Arithmetic)
preDecode i@(SUB  _ _ _) = (i, Arithmetic)
preDecode i@(SUBi _ _ _) = (i, Arithmetic)
preDecode i@(MUL  _ _ _) = (i, Arithmetic)
preDecode i@(CMP  _ _ _) = (i, Arithmetic)
preDecode i@(LW   _ _ _) = (i, LoadStore)
preDecode i@(LDC  _ _)   = (i, Arithmetic)
preDecode i@(SW   _ _ _) = (i, LoadStore)
preDecode i@(MOV  _ _)   = (i, Arithmetic)
preDecode i@(B _)        = (i, Branch)
preDecode i@(J _)        = (i, Branch)
preDecode i@(BLTH _ _ _) = (i, Branch)
preDecode i@NOOP         = (i, Misc)

-------------------------------------------------------------------------------
-- Decode 
-------------------------------------------------------------------------------

decodeIssue :: CPU (( Seq (HSIS ArchRegister, InstrType)
                    , ReorderBuffer ROBContents
                    , ReservationStation (ALUInstr Operand)
                    , RegisterAliasTable))
decodeIssue = do
  current <- ask
  issPerCycle <- askIssuePerCycle
  let cWindow = current ^. fpdUnit . window
      cROB    = current ^. rob
      cALUrs  = current ^. aluPL . aluRS
      cRAT  = current ^. rat
  decodeIssueRecursive issPerCycle cWindow cROB cALUrs cRAT

decodeIssueRecursive :: Int
                     -> Seq (HSIS ArchRegister, InstrType)
                     -> ReorderBuffer ROBContents
                     -> ReservationStation (ALUInstr Operand)
                     -> RegisterAliasTable
                     -> CPU (( Seq (HSIS ArchRegister, InstrType)
                             , ReorderBuffer ROBContents
                             , ReservationStation (ALUInstr Operand)
                             , RegisterAliasTable))
decodeIssueRecursive maxDepth cWindow cROB cALUrs cRAT
  | maxDepth <= 0 = pure (cWindow, cROB, cALUrs, cRAT)
  | otherwise = do
      maybeDecodeIssued <- decodeIssueSingle cWindow cROB cALUrs cRAT
      case maybeDecodeIssued of
        Nothing -> pure (cWindow, cROB, cALUrs, cRAT)
        Just (window', rob', aluRS', rat') ->
          decodeIssueRecursive (maxDepth - 1) window' rob' aluRS' rat'

decodeIssueSingle :: Seq (HSIS ArchRegister, InstrType)
                  -> ReorderBuffer ROBContents
                  -> ReservationStation (ALUInstr Operand)
                  -> RegisterAliasTable
                  -> CPU (Maybe ( Seq (HSIS ArchRegister, InstrType)
                                , ReorderBuffer ROBContents
                                , ReservationStation (ALUInstr Operand)
                                , RegisterAliasTable))
decodeIssueSingle cWindow cROB cRS cRAT = local (rat .~ cRAT) $ do
  case seqHead cWindow of
    Just (instruction, Arithmetic) -> do
      maybeDecodedInstr <- decodeArithmetic instruction
      let returnVal = do
            (dest, decodedInstr) <- maybeDecodedInstr
            (newROB, robEntry) <- allocateGetIndex (hsisToROBContents instruction) cROB
            rs' <- issue (robEntry, decodedInstr) cRS
            rat' <- Just $ B.insert dest robEntry cRAT
            Just (S.drop 1 cWindow, newROB, rs', rat')
      return returnVal
    _                              -> pure Nothing

decodeArithmetic :: HSIS ArchRegister -> CPU (Maybe (ArchRegister, ALUInstr Operand))
decodeArithmetic = foldHSIS funcs
  where
    funcs :: HSISFuncs ArchRegister (CPU (Maybe (ArchRegister, ALUInstr Operand)))
    funcs = HSISFuncs
      { add  = \x y z -> Just <$> decode2Reg AluADD x y z
      , addi = \x y z -> Just <$> decodeRegImm AluADDi x y z
      , sub  = \x y z -> Just <$> decode2Reg AluSUB x y z
      , subi = \x y z -> Just <$> decodeRegImm AluSUBi x y z
      , mul  = \x y z -> Just <$> decode2Reg AluMUL x y z
      , cmp  = \x y z -> Just <$> decode2Reg AluCMP x y z
      , lw   = \_ _ _ -> pure Nothing
      , ldc  = \dest x -> pure $ Just $ (dest, AluLDC x)
      , sw   = \_ _ _ -> pure Nothing
      , mov  = \x y -> Just <$> decodeReg AluMOV x y
      , b    = \_ -> pure Nothing
      , j    = \_ -> pure Nothing
      , blth = \_ _ _ -> pure Nothing
      , noop = pure Nothing
      }

decode2Reg :: (Operand -> Operand -> ALUInstr Operand)
          -> ArchRegister
          -> ArchRegister 
          -> ArchRegister
          -> CPU (ArchRegister, ALUInstr Operand)
decode2Reg constructor dest r1 r2 = do
  op1 <- askForOperand r1
  op2 <- askForOperand r2
  pure $ (dest, constructor op1 op2)

decodeRegImm :: (Operand -> Word64 -> ALUInstr Operand)
             -> ArchRegister
             -> ArchRegister 
             -> Word64
             -> CPU (ArchRegister, ALUInstr Operand)
decodeRegImm constructor dest r imm = do
  op1 <- askForOperand r
  pure $ (dest, constructor op1 imm)

decodeReg :: (Operand -> ALUInstr Operand)
          -> ArchRegister
          -> ArchRegister
          -> CPU (ArchRegister, ALUInstr Operand)
decodeReg constructor dest r = do
  op1 <- askForOperand r
  pure $ (dest, constructor op1)

-------------------------------------------------------------------------------
-- Dispatch 
-------------------------------------------------------------------------------

dispatchRS :: CPU (ReservationStation (ALUInstr Operand), [ALU])
dispatchRS = do
  rs   <- askRS
  alus <- askALUs
  dispatchRecursive rs alus

dispatchRecursive :: ReservationStation (ALUInstr Operand)
                  -> [ALU]
                  -> CPU (ReservationStation (ALUInstr Operand), [ALU])
dispatchRecursive rs alus = do
  maybeRSAlus <- dispatchSingle rs alus
  case maybeRSAlus of
    Nothing           -> pure (rs, alus)
    Just (rs', alus') -> dispatchRecursive rs' alus'

dispatchSingle :: ReservationStation (ALUInstr Operand)
               -> [ALU]
               -> CPU (Maybe (ReservationStation (ALUInstr Operand), [ALU]))
dispatchSingle rs alus =
  case dispatch rs of
    Nothing -> pure Nothing
    Just (rs', robAndInstr) ->
      case dispatchToFirstFreeALU robAndInstr alus of
        Nothing      -> pure Nothing
        Just (alus') -> pure $ Just $ (rs', alus')

dispatchToFirstFreeALU :: (ROBEntry, ALUInstr Word64) -> [ALU] -> Maybe [ALU]
dispatchToFirstFreeALU robAndInstr cALUs = case foldr f (False, []) cALUs of
                                       (True, alus') -> Just alus'
                                       (False, _)    -> Nothing
  where
    f :: ALU -> (Bool, [ALU]) -> (Bool, [ALU])
    f alu (True, alus)  = (True, alu : alus)
    f alu (False, alus) = case dispatchToALU robAndInstr alu of
                            Just alu' -> (True,  alu' : alus)
                            Nothing   -> (False, alu  : alus)

dispatchToALU :: (ROBEntry, ALUInstr Word64) -> ALU -> Maybe ALU
dispatchToALU instr alu
  | aluFree alu = Just (ALU (Just instr))
  | otherwise   = Nothing

aluFree :: ALU -> Bool
aluFree alu = not $ isJust $ _instr alu

-------------------------------------------------------------------------------
-- Execute 
-------------------------------------------------------------------------------

executeWriteback :: CPU ([ALU], CommonDataBus, ReservationStation (ALUInstr Operand), ReorderBuffer ROBContents)
executeWriteback = do
  (alus', cdb') <- execute
  (rs', rob') <- broadcastCDB cdb'
  pure (alus', cdb', rs', rob')

execute :: CPU ([ALU], CommonDataBus)
execute = do
  alus <- askALUs
  let xs    = map execALU alus
      alus' = map fst xs
      cdb'  = CDB [ fromJust (snd x) | x <- xs, isJust (snd x)]
  pure (alus', cdb')

execALU :: ALU -> (ALU, Maybe (ROBEntry, Word64))
execALU (ALU (Just (robEntry, instr))) = (initialise, Just (robEntry, evalALU instr))
execALU alu@(ALU Nothing)              = (alu, Nothing)

evalALU :: ALUInstr Word64 -> Word64
evalALU (AluADD  x y) = x + y
evalALU (AluADDi x y) = x + y
evalALU (AluSUB  x y) = x - y
evalALU (AluSUBi x y) = x - y
evalALU (AluMUL  x y) = x * y
evalALU (AluCMP  x y)
  | x <  y = w64 (-1 :: Int)
  | x == y = 0
  | x >  y = 1
evalALU (AluLDC imm)  = imm
evalALU (AluMOV x)    = x

-------------------------------------------------------------------------------
-- Writeback 
-------------------------------------------------------------------------------

broadcastCDB :: CommonDataBus -> CPU (ReservationStation (ALUInstr Operand), ReorderBuffer ROBContents)
broadcastCDB (CDB cCDB) = do
  cROB <- askROB
  cALUrs <- askRS
  let rs' = applyAll cALUrs [broadcast robEntry x | (robEntry, x) <- cCDB]
      rob' = applyAll cROB $ map broadcastROB cCDB
  pure (rs', rob')  
  
broadcastROB :: (ROBEntry, Word64)
             -> ReorderBuffer ROBContents
             -> ReorderBuffer ROBContents
broadcastROB (robEntry, x) rob = adjustROB replaceROBwithVal robEntry rob
  where
  replaceROBwithVal :: ROBContents -> ROBContents
  replaceROBwithVal (ROBContents r Nothing) = ROBContents r (Just x)
  replaceROBwithVal (ROBContents r v)       = ROBContents r v

-------------------------------------------------------------------------------
-- Commit 
-------------------------------------------------------------------------------

commit :: CPU (Registers, RegisterAliasTable, ReorderBuffer ROBContents)
commit = do
  cRegs <- askRegs
  cRAT  <- askRAT
  cROB  <- askROB
  issPerCycle <- askIssuePerCycle
  commitRecursive issPerCycle cRegs cRAT cROB

commitRecursive :: Int
                -> Registers
                -> RegisterAliasTable
                -> ReorderBuffer ROBContents
                -> CPU (Registers, RegisterAliasTable, ReorderBuffer ROBContents)
commitRecursive maxDepth cRegs cRAT cROB
  | maxDepth <= 0 = pure (cRegs, cRAT, cROB)
  | otherwise = do
    m <- commitSingle cRegs cRAT cROB
    case m of
      Nothing -> pure (cRegs, cRAT, cROB)
      Just (regs', rat', rob') ->
        commitRecursive (maxDepth - 1) regs' rat' rob'

commitSingle :: Registers
             -> RegisterAliasTable
             -> ReorderBuffer ROBContents
             -> CPU (Maybe (Registers, RegisterAliasTable, ReorderBuffer ROBContents))
commitSingle cRegs cRAT cROB = do
  let (rob', mx) = retire cROB
  case mx of
    Nothing -> pure Nothing
    Just (ROBContents r Nothing, robEntry)  -> pure Nothing
    Just (ROBContents r (Just v), robEntry) ->
      let regs' = update r v cRegs
          rat'  = case B.lookup r cRAT of
                    Nothing -> cRAT
                    Just robEntry'  -> if robEntry' == robEntry 
                                         then B.delete r cRAT
                                         else cRAT
      in pure $ Just (regs', rat', rob')

initProcStateWithInstructions :: [HSIS ArchRegister] -> ProcState
initProcStateWithInstructions instructions = initialise {_instrCache=S.fromList instructions}

example :: IO (Int, ProcState)
example = do
  r <- readInstructions "testPrograms/factorialUnroll.asm"
  pure $ tick $ tick $ tick $ tick $ tick $ tick $ tick (0, initProcStateWithInstructions r)

exampleDebug :: IO ()
exampleDebug = do
  r <- readInstructions "testPrograms/factorialUnroll.asm"
  ps' <- printTick (0, initProcStateWithInstructions r)
  ps'' <- printTick ps'
  ps''' <- printTick ps''
  ps'''' <- printTick ps'''
  ps''''' <- printTick ps''''
  let debugVal = runReader dispatchRS (snd ps''''')
  prettyPrint $ fst debugVal
  prettyPrint $ snd debugVal

-- Running the simulator

simulateFilePrintStepsStatsConfig :: Int -> Int -> FilePath -> IO () 
simulateFilePrintStepsStatsConfig numWaySuperscalar numALUs file = do
  r <- readInstructions file
  let alus = take numALUs (repeat initialise)
      procStart = (aluPL . aluEUs) .~ alus
                $ issuePerCycle .~ numWaySuperscalar
                $ initialise
  (numInstr, numCycles, ipc, steps) <- simulateFileStepsStatsInit file procStart
  traverse_ prettyPrint steps
  putStrLn "\n"
  putStrLn $ "Number of instructions: " ++ show numInstr
  putStrLn $ "Number of cycles:       " ++ show numCycles
  putStrLn $ "Instructions per Cycle: " ++ show ipc

simulateFile :: FilePath -> IO ProcState
simulateFile file = do
  r <- readInstructions file
  return $ simulate r

simulateFileSteps :: FilePath -> IO [ProcState]
simulateFileSteps file = do
  r <- readInstructions file
  return $ simulateSteps r

simulateFileStatsInit :: FilePath -> ProcState -> IO (Int, Int, Double)
simulateFileStatsInit file start = do
  r <- readInstructions file
  let (numExec, numCycles, instrPerCycle, _) = simulateStepsStatsInit r start
  return (numExec, numCycles, instrPerCycle)

simulateFileTaggedStatsInit :: FilePath -> (Int, ProcState) -> IO (Int, Int, Int, Double)
simulateFileTaggedStatsInit file (tag, start) = do
  r <- readInstructions file
  let (numExec, numCycles, instrPerCycle, _) = simulateStepsStatsInit r start
  return (tag, numExec, numCycles, instrPerCycle)

simulateFileStepsStatsInit :: FilePath -> ProcState -> IO (Int, Int, Double, [ProcState])
simulateFileStepsStatsInit file start = do
  r <- readInstructions file
  return $ simulateStepsStatsInit r start

simulateFilePrintSteps :: FilePath -> IO ()
simulateFilePrintSteps file = do
  states <- simulateFileSteps file 
  traverse_ prettyPrint states

simulate :: [HSIS ArchRegister] -> ProcState
simulate is = head
            $ filter halted
            $ map snd (iterate tick (0, loadInstrs initialise is))

simulateSteps :: [HSIS ArchRegister] -> [ProcState]
simulateSteps is = takeUntil halted
                 $ map snd (iterate tick (0, loadInstrs initialise is))

simulateStepsInit :: [HSIS ArchRegister] -> ProcState -> [ProcState]
simulateStepsInit is start = takeUntil halted
                           $ map snd (iterate tick (0, loadInstrs start is))

simulateStepsStats :: [HSIS ArchRegister] -> (Int, Int, Double, [ProcState])
simulateStepsStats is = (numExec, numCycles, instrPerCycle, steps)
  where 
    steps = simulateSteps is
    numExec = length is
    numCycles = length steps
    instrPerCycle = fromIntegral numExec / fromIntegral numCycles

simulateStepsStatsInit :: [HSIS ArchRegister] -> ProcState -> (Int, Int, Double, [ProcState])
simulateStepsStatsInit is start = (numExec, numCycles, instrPerCycle, steps)
  where 
    steps = simulateStepsInit is start
    numExec = length is
    numCycles = length steps
    instrPerCycle = fromIntegral numExec / fromIntegral numCycles

-- Experiments

experiment :: String -> [(Int,ProcState)] -> IO ()
experiment tag procstates = traverse_ (experimentTrial procstates) foo
  where
    foo = filesToTest tag [ "factorialUnroll" 
                          , "RAWDependencies"
                          , "WAWDependencies"
                          , "WARDependencies"
                          ]

filesToTest :: String -> [String] -> [(FilePath, FilePath)]
filesToTest tag = map (\name -> prependDirs (name ++ ".asm", name))
  where
    prependDirs :: (FilePath, FilePath) -> (FilePath, FilePath)
    prependDirs (x, y) = ( "testPrograms/" ++ x
                          , "graphs/" ++ y ++ "." ++ tag)

experimentTrial :: [(Int,ProcState)] -> (FilePath, FilePath) -> IO ()
experimentTrial procs (inputFile, outputFile) = do
  stats <- traverse (simulateFileTaggedStatsInit inputFile) procs
  print stats
  writeFile outputFile (formatData stats)

formatData :: [(Int, Int, Int, Double)] -> String
formatData = unlines . map format
  where
    format :: (Int, Int, Int, Double) -> String
    format (n, x, y, z) = show n ++ " "
                       ++ show x ++ " "
                       ++ show y ++ " "
                       ++ show z


experimentALUs :: IO ()
experimentALUs = do
  let x = [ (numALUs, (aluPL . aluEUs) .~ alus $ initialise) 
              | numALUs <- [1 .. 6]
              , let alus = take numALUs (repeat initialise)]
  experiment "alus" x

experimentDependencies :: IO ()
experimentDependencies = experiment "dependencies" [(4,initialise)]

experimentNumIssued :: IO ()
experimentNumIssued = do
  let foo = [ (numWaySuperscalar, procState)
              | numWaySuperscalar <- [1..16]
              , let alus = take numWaySuperscalar (repeat initialise)
                    procState = (aluPL . aluEUs) .~ alus
                              $ issuePerCycle .~ numWaySuperscalar
                              $ initialise
              ]
  experiment "issuePerCycle" foo
