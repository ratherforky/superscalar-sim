{-# LANGUAGE TypeApplications #-}
module Parser where

import Types
import Utilities (isEmpty)
import Pretty

import Data.Int
import Data.List (dropWhileEnd)
import Data.List.Extra (replace)
import Data.Map.Strict (Map)
import Data.Sequence (Seq)

import qualified Data.Map.Strict as M
import qualified Data.Sequence as S

import Text.Yoda

-- BNF taken from http://homepage.divms.uiowa.edu/~jones/syssoft/notes/02assem.html

program :: Parser Program
program = Program <$> some line

line :: Parser Line
line = Def <$> macro <* tok "\n"
    <|>Stm <$> statement <* tok "\n"
    <|>Com <$> comment <* tok "\n"-- Implies newline
    <|>Empty <$ tok "\n"

macro :: Parser Macro
macro = Macro <$> var <* tok "=" <*> num

comment :: Parser Comment
comment = tok "//" *> some (satisfy (\c -> c /= '\n' && elem c [' ' .. '~']))

statement :: Parser Statement
statement = Labled <$> var <* tok ":" <*> instruction
         <|>Unlabled <$ whitespace <*> instruction

instruction :: Parser Instruction
instruction = PADDi <$ tok "ADDi" <*> reglike <*> reglike <*> numOrVar
           <|>PADD  <$ tok "ADD" <*> reglike <*> reglike <*> reglike
           <|>PSUBi  <$ tok "SUBi" <*> reglike <*> reglike <*> numOrVar
           <|>PSUB <$ tok "SUB" <*> reglike <*> reglike <*> reglike
           <|>PMUL  <$ tok "MUL" <*> reglike <*> reglike <*> reglike
           <|>PDIV  <$ tok "DIV" <*> reglike <*> reglike <*> reglike
           <|>PCMP  <$ tok "CMP" <*> reglike <*> reglike <*> reglike
           <|>PLW   <$ tok "LW" <*> reglike <*> reglike <*> reglike
           <|>PLDC  <$ tok "LDC" <*> reglike <*> numOrVar
           <|>PSW   <$ tok "SW" <*> reglike <*> reglike <*> reglike
           <|>PMOV  <$ tok "MOV" <*> reglike <*> reglike
           <|>PB    <$ tok "B" <*> numOrVar
           <|>PJ    <$ tok "J" <*> num
           <|>PBLTH <$ tok "BLTH" <*> reglike <*> reglike <*> numOrVar
           <|>PNOOP <$ tok "NOOP"

reglike :: Parser Reglike
reglike = Left  <$> register <* whitespace
       <|>Right <$> var

numOrVar :: (Num a, Read a) => Parser (Either a Identifier)
numOrVar = Left <$> num
        <|>Right <$> var

register :: Parser ArchRegister
register = R0 <$ tok "R0"
        <|>R1 <$ tok "R1"
        <|>R2 <$ tok "R2"
        <|>R3 <$ tok "R3"
        <|>R4 <$ tok "R4"
        <|>R5 <$ tok "R5"
        <|>R6 <$ tok "R6"
        <|>R7 <$ tok "R7"
        <|>R8 <$ tok "R8"
        <|>R9 <$ tok "R9"
        <|>R10 <$ tok "R10"
        <|>R11 <$ tok "R11"
        <|>R12 <$ tok "R12"
        <|>SP <$ tok "SP"
        <|>LR <$ tok "LR"

-- Helper parsers

chainl p op = p >>= rest where
  rest x = do f <- op
              y <- p
              rest (f x y)
        <|> return x

num :: (Num a, Read a) => Parser a
num = hex <* whitespace 
   <|>decimal <* whitespace

decimal :: (Num a, Read a) => Parser a
decimal = read <$> some (oneOf ['0' .. '9'])

hex :: (Num a, Read a) => Parser a
hex = read <$> ((++) <$> optionalParse (string "-") 
                     <*> string "0x" 
      <**> pure (++) <*> some (oneOf (['0' .. '9'] ++ ['a' .. 'f'])))

var :: Parser String
var = some (oneOf ['a' .. 'z']) <* whitespace

whitespace :: Parser ()
whitespace = () <$ many (oneOf " \t\n\r")

tok :: String -> Parser String
tok t = whitespace *> string t <* whitespace

optionalParse :: Monoid a => Parser a -> Parser a
optionalParse p = p <|> pure mempty

-- AdvancedParsing

-- readInstructions' :: FilePath -> IO [HSIS ArchRegister]
-- readInstructions' file = do
--   r <- readFile file
--   return 

-- Basic

readInstructions :: FilePath -> IO [HSIS ArchRegister]
readInstructions file = do
  r <- readFile file
  return $ map (read @(HSIS ArchRegister))
         $ lexer
         $ filter (\xs -> not ((isEmpty xs) || (elem '#' xs)))
         $ lines r

lexer :: [String] -> [String]
lexer xs = removeLabels
         $ replaceLabels
         $ map (\(x,_,z) -> (x,z))
         $ S.foldlWithIndex addLabel []
         $ S.fromList xs
  where
    replaceLabels :: [(Int, String)] -> [String]
    replaceLabels = foldr f xs

    f :: (Int, String) -> [String] -> [String]
    f (i, label) = map (replace label (show i))

addLabel :: [(Int, Int, String)] -> Int -> String -> [(Int, Int, String)]
addLabel [] i line
  | ':' `elem` line = [(i, 1, removeColon line)]
  | otherwise       = []
addLabel ys@((_, numLabelsSoFar, _):_) i line
  | ':' `elem` line = (i-numLabelsSoFar, numLabelsSoFar+1, removeColon line) : ys
  | otherwise       = ys

addLabel' :: Int -> String -> [(Int, Int, String)] -> [(Int, Int, String)]
addLabel' i line ys
  | ':' `elem` line = (i, 1, removeColon line):ys
  | otherwise       = ys

removeColon :: String -> String
removeColon = dropWhileEnd (\c -> (c == ':') || (c == ' '))

removeLabels :: [String] -> [String]
removeLabels = filter (not . elem ':')

foldProgram :: (Line -> b -> b) -> b -> Program -> b
foldProgram f k (Program xs) = foldr f k xs

type MacroMap = Map Identifier Int32
type RegMap   = Map Identifier ArchRegister
type LabelMap = Map Label      Word64
type Labels   = Map Label      ()

macros :: Program -> MacroMap
macros = foldProgram f M.empty
  where
    f :: Line -> MacroMap -> MacroMap
    f (Def (Macro identifier x)) mm = M.insert identifier x mm
    f _ mm = mm

collectLabels :: Program -> Labels
collectLabels = foldProgram f M.empty
  where
    f :: Line -> Labels -> Labels
    f (Stm (Labled label _)) lm = M.insert label () lm
    f _ lm = lm

