module Data.Memory where

import Data.IntMap.Strict (IntMap)
import qualified Data.IntMap.Strict as IM
import Data.Word (Word64)

newtype Memory = Mem (IntMap Word64) deriving Show

foldMemory :: (Int -> Word64 -> b -> b) -> b -> Memory -> b
foldMemory f k (Mem imap) = IM.foldrWithKey f k imap

empty :: Memory
empty = Mem IM.empty

lookup :: Int -> Memory -> Word64
lookup i (Mem imap) = case IM.lookup i imap of
                        Just w  -> w
                        Nothing -> 0 -- If no entry, memory address is 0