{-# LANGUAGE ScopedTypeVariables #-}
module Data.ReorderBuffer 
  ( ReorderBuffer, ROBEntry
  , emptyROBWithSize, full, isEmpty
  , allocate, allocateList, allocateGetIndex
  , peek, retire, adjustROB, indexROB
  , foldrWithEntry
  ) where

import Data.Foldable (foldl')
import Data.Sequence (Seq, (|>))
import Data.Sequence as S

type ROBEntry = Integer

data ReorderBuffer a = RB (Seq a) Int Integer

instance Functor ReorderBuffer where
  fmap f (RB buf size offset) = RB (fmap f buf) size offset

instance Foldable ReorderBuffer where
--foldr :: (a -> b -> b) -> b -> ReorderBuffer a -> b
  foldr f k (RB seq _ _) = foldr f k seq

foldrWithEntry :: forall a b . (ROBEntry -> a -> b -> b) -> b -> ReorderBuffer a -> b
foldrWithEntry f k (RB seq size offset) = S.foldrWithIndex g k seq
  where
    g :: Int -> a -> b -> b
    g i = f (fromIntegral i + offset)

instance Show a => Show (ReorderBuffer a) where
  show (RB buf size _) = mconcat 
    ["Reorderbuffer with size ", show size, ": ", show buf]

emptyROBWithSize :: Int -> ReorderBuffer a
emptyROBWithSize size = RB S.empty size 0

isEmpty :: ReorderBuffer a -> Bool
isEmpty (RB s _ _) = S.null s

full :: ReorderBuffer a -> Bool
full (RB buf size _)
  | S.length buf == size = True
  | S.length buf <  size = False
  | S.length buf >  size = error "Buffer cannot be bigger than specified size"

allocate :: a -> ReorderBuffer a -> Maybe (ReorderBuffer a) 
allocate x rob@(RB buf size offset)
  | full rob  = Nothing
  | otherwise = Just $ RB buf' size offset
  where
    buf' = buf |> x

allocateGetIndex :: a -> ReorderBuffer a -> Maybe (ReorderBuffer a, ROBEntry) 
allocateGetIndex x rob@(RB buf size offset)
  | full rob  = Nothing
  | otherwise = Just $ (RB buf' size offset, offset + toInteger (S.length buf') - 1)
  where
    buf' = buf |> x

allocateList' :: [a] -> ReorderBuffer a -> Maybe (ReorderBuffer a)
allocateList' []     rob = Just rob
allocateList' (x:xs) rob = do
  rob' <- allocate x rob
  allocateList' xs rob'

allocateList :: Foldable t => ReorderBuffer a -> t a -> Maybe (ReorderBuffer a)
allocateList rob = foldl' f (Just rob)
  where
    f :: Maybe (ReorderBuffer a) -> a -> Maybe (ReorderBuffer a)
    f acc x = acc >>= allocate x

retire_ :: ReorderBuffer a -> ReorderBuffer a
retire_ (RB buf size offset) =
  case S.viewl buf of
    EmptyL    -> emptyROBWithSize size
    x :< buf' -> RB buf' size (offset + 1)

retire :: ReorderBuffer a -> (ReorderBuffer a, Maybe (a, ROBEntry))
retire (RB buf size offset) =
  case S.viewl buf of
    EmptyL    -> (emptyROBWithSize size, Nothing)
    x :< buf' -> (RB buf' size (offset + 1), Just (x, offset))

peek :: ReorderBuffer a -> Maybe a
peek (RB buf size _) = 
  case S.viewl buf of
    EmptyL    -> Nothing
    x :< buf' -> Just x

-- Index ROB with perma references
indexROB :: ReorderBuffer a -> ROBEntry -> Maybe a
indexROB (RB buf _ offset) i
  | iOffset >= S.length buf = Nothing
  | iOffset < 0             = Nothing
  | otherwise               = Just $ S.index buf iOffset
  where
    iOffset = fromInteger $ i - offset

adjustROB :: (a -> a) -> ROBEntry -> ReorderBuffer a -> ReorderBuffer a
adjustROB f i rob@(RB buf size offset)
  | iOffset >= S.length buf = rob
  | iOffset < 0             = rob
  | otherwise               = RB (S.adjust' f iOffset buf) size offset
  where
    iOffset = fromInteger $ i - offset
