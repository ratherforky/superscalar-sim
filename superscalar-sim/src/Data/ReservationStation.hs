{-# LANGUAGE FlexibleInstances #-}
module Data.ReservationStation 
  ( ReservationStation(..)
  , Operand(..), ALUInstr(..)
  , emptyRSWithSize, issue, find, dispatch, broadcast
  , foldRS
  , testRS
  ) where

import Data.IntMap.Strict (IntMap)
import qualified Data.IntMap.Strict as IM
import Data.List (insert)
import Data.ReorderBuffer (ROBEntry)
import Data.Word (Word64)

import Lens.Micro (_2, (%~))

data ReservationStation a = RS [Int] -- Free slots in reservation station
                               (IntMap (ROBEntry, a))
  deriving Show

instance Functor ReservationStation where
  fmap f (RS freeSlots imap) = RS freeSlots (fmap (_2 %~ f) imap)

instance Foldable ReservationStation where
--foldr :: (a -> b -> b) -> b -> ReservationStation a -> b
  foldr f k (RS _ imap) = foldr (f . snd) k imap

foldRS :: ((ROBEntry, a) -> b -> b) -> b -> ReservationStation a -> b
foldRS f k (RS _ imap) = foldr f k imap

class Ready a where
  ready :: a -> Bool

instance Ready b => Ready (a,b) where
--ready :: ALUInstr -> Bool
  ready = ready . snd

instance Foldable t => Ready (t Operand) where
  --ready :: ALUInstr -> Bool
    ready = foldr (\op acc -> val op && acc) True
      where
        val (Val _) = True
        val _       = False

-- Unsafe in that the specialisation function is partial
-- Only to be used when value of all operands are available 
class UnsafeSpecialise i where
  unsafeSpecialise :: i Operand -> i Word64

-- Repeated boilerplate to avoid Undecidable instances
instance UnsafeSpecialise ALUInstr where
  unsafeSpecialise = fmap (\(Val x) -> x)

instance UnsafeSpecialise BUInstr where
  unsafeSpecialise = fmap (\(Val x) -> x)

instance UnsafeSpecialise LSInstr where
  unsafeSpecialise = fmap (\(Val x) -> x)

data Operand = Val Word64   -- Got value and can use it immediately
             | ROB ROBEntry -- Relies on completion of instruction tagged with Int
             deriving (Show, Eq)

data ALUInstr operand
  = AluADD  operand operand
  | AluADDi operand Word64
  | AluSUB  operand operand
  | AluSUBi operand Word64
  | AluMUL  operand operand
  | AluCMP  operand operand
  | AluLDC  Word64
  | AluMOV  operand
  deriving (Eq, Show)

instance Functor ALUInstr where
  fmap f (AluADD  x y)   = AluADD  (f x) (f y)
  fmap f (AluADDi x imm) = AluADDi (f x) imm
  fmap f (AluSUB  x y)   = AluSUB  (f x) (f y)
  fmap f (AluSUBi x imm) = AluSUBi (f x) imm
  fmap f (AluMUL  x y)   = AluMUL  (f x) (f y)
  fmap f (AluCMP  x y)   = AluCMP  (f x) (f y)
  fmap f (AluLDC imm)    = AluLDC  imm
  fmap f (AluMOV x)      = AluMOV  (f x)

instance Foldable ALUInstr where
  foldr f k (AluADD  x y) = f x (f y k)
  foldr f k (AluADDi x y) = f x k
  foldr f k (AluSUB  x y) = f x (f y k)
  foldr f k (AluSUBi x y) = f x k
  foldr f k (AluMUL  x y) = f x (f y k)
  foldr f k (AluCMP  x y) = f x (f y k)
  foldr f k (AluLDC imm)  = k
  foldr f k (AluMOV x)    = f x k

data BUInstr operand
  = Abs  Word64
  | Rel  Word64
  | Cond operand operand Word64
  deriving (Eq, Show)

instance Functor BUInstr where
  fmap _ (Abs imm)      = Abs imm
  fmap _ (Rel imm)      = Rel imm
  fmap f (Cond x y imm) = Cond (f x) (f y) imm

instance Foldable BUInstr where
  foldr f k (Abs x)        = k
  foldr f k (Rel x)        = k
  foldr f k (Cond x y imm) = f x (f y k)

-- Load/Store instructions
data LSInstr operand
  = Load  operand operand operand
  | Store operand operand operand
  deriving (Eq, Show)

instance Functor LSInstr where
  fmap f (Load  x y z) = Load  (f x) (f y) (f z)
  fmap f (Store x y z) = Store (f x) (f y) (f z)

instance Foldable LSInstr where
  foldr f k (Load  x y z) = f x (f y (f z k))
  foldr f k (Store x y z) = f x (f y (f z k))

-------------------------------------------------------------------------------
-- API
-------------------------------------------------------------------------------

emptyRSWithSize :: Int -> ReservationStation a
emptyRSWithSize max
  | max <= 0  = error "Can't have Reservation Station with 0 or fewer slots"
  | otherwise = RS [0..max-1] IM.empty

issue :: (ROBEntry, a) -> ReservationStation a -> Maybe (ReservationStation a)
issue x (RS [] imap)           = Nothing -- No free slots, can't issue
issue x (RS (slot:slots) imap) = Just $ RS slots (IM.insert slot x imap)

-- Finds first instruction that's ready and returns it with its index and ROBEntry
find :: UnsafeSpecialise instr 
     => (instr Operand -> Bool)
     -> ReservationStation (instr Operand)
     -> Maybe (Int, (ROBEntry, instr Word64))
find p (RS _ imap)= IM.foldlWithKey' f Nothing imap
  where
  --f :: Maybe (Int, (ROBEntry, instr Word64)) -> Int -> (ROBEntry, a) -> Maybe (Int, (ROBEntry, instr Word64))
    f Nothing i x@(_,y)    = if p y then Just (i, _2 %~ unsafeSpecialise $ x) else Nothing
    f justResult _ _ = justResult    

-- Monomorphic example:
-- dispatch :: ReservationStation (ALUInstr Operand)
--          -> Maybe (ReservationStation (ALUInstr Operand), (ROBEntry, ALUInstr Word64))
dispatch :: (UnsafeSpecialise instr, Ready (instr Operand)) 
         => ReservationStation (instr Operand)
         -> Maybe (ReservationStation (instr Operand), (ROBEntry, instr Word64))
dispatch rs@(RS freeSlots imap)
  | IM.null imap       = Nothing -- No instructions to dispatch
  | otherwise = 
    case find ready rs of
      Nothing              -> Nothing
      Just (i, toDispatch) -> Just $ ( RS (insert i freeSlots) 
                                          (IM.delete i imap)
                                     , toDispatch)

broadcast :: Functor f
          => ROBEntry
          -> Word64
          -> ReservationStation (f Operand)
          -> ReservationStation (f Operand)
broadcast robEntry value rs = fmap (fmap replaceROBwithVal) rs
  where
    replaceROBwithVal :: Operand -> Operand
    replaceROBwithVal (Val x) = Val x
    replaceROBwithVal (ROB entryWaitingOn)
      | robEntry == entryWaitingOn = Val value
      | otherwise                  = ROB entryWaitingOn

testRS = RS [] (IM.fromList [(0,(1,AluLDC 10)),(1,(2,AluLDC 1)),(2,(3,AluSUB (Val 0) (Val 0))),(3,(4,AluSUBi (Val 0) 1)),(4,(5,AluSUB (Val 0) (Val 0))),(5,(6,AluSUBi (Val 0) 1)),(6,(7,AluSUB (Val 0) (Val 0))),(7,(8,AluSUBi (Val 0) 1)),(8,(9,AluSUB (Val 0) (Val 0))),(9,(10,AluSUBi (Val 0) 1)),(10,(11,AluSUB (Val 0) (Val 0))),(11,(12,AluSUBi (Val 0) 1)),(12,(13,AluSUB (Val 0) (Val 0))),(13,(14,AluSUBi (Val 0) 1)),(14,(15,AluSUB (Val 0) (Val 0))),(15,(16,AluSUBi (Val 0) 1))])

example :: Maybe (ReservationStation (ALUInstr Operand))
example = issue (1, AluADD (Val 5) (Val 10)) (emptyRSWithSize 10) 
      >>= issue (2, AluSUB (Val 100) (ROB 1))

exampleSequence :: Maybe (ReservationStation (ALUInstr Operand), (ROBEntry, ALUInstr Word64))
exampleSequence = do
  rs <- example
  (rs', (rob, _)) <- dispatch rs
  let resultFromExecution = 75 -- Mocked up here
  dispatch $ broadcast rob resultFromExecution rs'
